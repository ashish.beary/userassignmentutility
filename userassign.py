import subprocess
import os
from PySide.QtCore import *
from PySide import QtGui
from PySide import QtCore
import sys
import threading
import psycopg2


class assign(QtGui.QWidget):

    def activate(self):
	self.b1.setEnabled(True)
        self.b1.setStyleSheet("QWidget { color:#323B44; alignment: center; font-size: 15px; margin-top: 5px; background-color: #3BAFDA; width: 60px; height: 35px; border-radius: 5px; font-weight: 600; font-size: 16px;}")
        self.l3.setText('')
	self.in1.setText("''")
	#self.in2.setText('')
        

    def assignUser(self):
        self.b1.setEnabled(False)
        self.b1.setStyleSheet("QWidget { color:#323B44; alignment: center; font-size: 15px; margin-top: 5px; background-color: #36404A; width: 100px; height: 35px; border-radius: 5px; font-weight: 600; font-size: 16px;}")
        QtGui.qApp.processEvents()
        print("assigning mahn, wait")
        if self.in1.text()!="": #and self.in2.text()!="":
            self.dbcon() 

        else:
            err = QtGui.QErrorMessage(self)
            err.showMessage("All the fields are mandatory")
            self.b1.setEnabled(True)
            self.b1.setStyleSheet("QWidget { color:#323B44; alignment: center; font-size: 15px; margin-top: 5px; background-color: #3BAFDA; width: 60px; height: 35px; border-radius: 5px; font-weight: 600; font-size: 16px;}")
            self.l3.setText('')

    def dbcon(self):
        c = psycopg2.connect(host="ecndbctrl",database="ecnsessionbroker_broker_db", user="postgres") #fetch from yaml file
        cur = c.cursor()
        cur.execute("select id from users where username={}".format(self.in1.text())) #sql commands mask
        x = cur.fetchone()
        if x is None:
            print('user not found')
            e = "User not found in the database"
            err = QtGui.QErrorMessage(self)
            err.showMessage(e)
	    self.activate()
        else:
            print("user found")
	    print(type(x))
	    print(x)
            cur.execute("select instance_id from user_instance_assignment where user_id=%s",x)
            y = cur.fetchone()
            if y is None:
                print("Instance not found")
                e = "No instance has been assigned to the user. Please assign the user to any instance and then proceed."
		err = QtGui.QErrorMessage(self)
            	err.showMessage(e)
	        self.activate()
            else:
                print("Instance found")
                y = str(y)
                cur.execute("select user_id from users_roles where user_id=%s",x)
                z = cur.fetchone()
                if z is None:
                    print("Cannot Update")
	            self.activate()
                else:
		    cT = self.comboBox.currentText() #get user list from dropdrown
		    temp = self.cB.checkState()
		    print(str(temp))
		    print(cT)
                    if(cT == 'Internal' ):
                        cur.execute("update users_roles set role_id=24 where user_id=%s",x)
                        c.commit()
        	        self.activate()
                        print("Internal Role Assigned")
	                i = "Internal Role Assigned"
                        imsg = QtGui.QErrorMessage(self)
          	        imsg.showMessage(i)
                    elif(cT == 'External'):
                        cur.execute("update users_roles set role_id=26 where user_id=%s",x)
                        c.commit()
			self.activate()
	                print("External Role Assigned")
		        i = "External Role Assigned"
                        imsg = QtGui.QErrorMessage(self)
  	                imsg.showMessage(i)
			self.activate()
                    elif(cT == 'Web'):
                        cur.execute("update users_roles set role_id=25 where user_id=%s",x)
                        c.commit()
                        self.activate()
                        print("Web Role Assigned")
                        i = "Web Role Assigned"
                        imsg = QtGui.QErrorMessage(self)
                        imsg.showMessage(i)
                        self.activate()



    def __init__(self):
        super(assign, self).__init__()
        
        l = QtGui.QLabel(self)
        l.setPixmap(QtGui.QPixmap(".louvre2.png"))
        l.setStyleSheet("QWidget { margin-left: 1px; margin-top: 1px; }")
        l.setFixedWidth(70)
        l.setFixedHeight(70)

        t = QtGui.QLabel("\nVDI Management Utility\n")
        t.setStyleSheet("QWidget { color: #D2AE60; font-size: 21px; font-weight: 600;}")
        t.setAlignment(Qt.AlignLeft)
        t.setFixedHeight(70)

        v = QtGui.QVBoxLayout()
        h = QtGui.QHBoxLayout()
        
	h.addWidget(l)
	h.addWidget(t)
        v.addLayout(h)

        h = QtGui.QHBoxLayout()
        self.l1 = QtGui.QLabel("Username")
        self.l1.setStyleSheet("QWidget { margin-left: 1px; color: #D2AE60; alignment: center; background-color: #36404A; font-size: 17px; border-radius: 5px;}")
        self.l1.setAlignment(Qt.AlignCenter)
        self.l1.setFixedWidth(200)
        self.l1.setFixedHeight(30)

        self.in1 = QtGui.QLineEdit("")
        self.in1.setStyleSheet("QWidget { color: #000000; alignment: center; background-color: #ffffff; font-size: 16px; border-radius: 5px;}")
        self.in1.setAlignment(Qt.AlignCenter)
        self.in1.setFixedHeight(30)

        h.addWidget(self.l1)
        h.addWidget(self.in1)
        v.addLayout(h)

        h = QtGui.QHBoxLayout()
        self.l2 = QtGui.QLabel("Access")
        self.l2.setStyleSheet("QWidget { color: #D2AE60; alignment: center; background-color: #36404A; font-size: 17px; border-radius: 5px;}")
        self.l2.setAlignment(Qt.AlignCenter)
        self.l2.setFixedWidth(200)
        self.l2.setFixedHeight(30)
        self.comboBox = QtGui.QComboBox(self)
        self.comboBox.setStyleSheet("QWidget { color:#D2AE60; font-size: 15px; margin-top: 5px; background-color: #36404A; text-align: center; width: 140px; height: 30px;  border-radius: 5px; font-size: 16px;}")
        self.comboBox.addItem('External')
        self.comboBox.addItem('Internal')
	self.comboBox.addItem('Web')
        self.comboBox.move(5, 5)

        h.addWidget(self.l2)
        h.addWidget(self.comboBox)
        v.addLayout(h)
	h.addStretch(0)

        h = QtGui.QHBoxLayout()
        v.addLayout(h)

        self.b1 = QtGui.QPushButton("Assign User")
        self.b1.setStyleSheet("QWidget { color:#323B44; alignment: center; font-size: 15px; margin-top: 5px; background-color: #3BAFDA; width: 60px; height: 35px; border-radius: 5px; font-weight: 600; font-size: 16px;}")
	self.b1.setFixedWidth(200)	

        self.l3 = QtGui.QLabel('')
        self.l3.setStyleSheet("QWidget { color:#D2AE60; alignment: center; background-color:#2f363d; font-size: 18px; border-radius: 5px;}")
        self.l3.setAlignment(Qt.AlignCenter)
        self.l3.setFixedHeight(20)
        
        h.addWidget(self.b1)
        h.addWidget(self.l3)
        self.b1.clicked.connect(self.assignUser)
        
	
	self.cB = QtGui.QCheckBox("MFA", self)
	self.cB.stateChanged.connect(self.dbcon)
	h.addWidget(self.cB)
	

	self.setStyleSheet("color: #ffffff;"
                    "background-color: #2f363d;"
                    "selection-color: yellow;"
                    "selection-background-color: blue;"
                    "font-weight: 300;")

        self.setLayout(v)
        self.setGeometry(0,0,300,150)
        q = self.frameGeometry()
        c = QtGui.QDesktopWidget().availableGeometry().center()
        q.moveCenter(c)
        self.move(q.topLeft())
        self.setWindowTitle('Powered By EnCloudEn')
        self.show()

    def closeEvent(self, event):

        ask = QtGui.QMessageBox.question(self, 'Quit', "Are you sure to quit?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if ask == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    ob = assign()
    sys.exit(app.exec_())




