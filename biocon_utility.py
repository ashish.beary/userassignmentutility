import os, subprocess, yaml, time, threading, logging, sys
from PySide.QtCore import *
from PySide import QtGui
from PySide import QtCore
from datetime import datetime

class VdiManagement(QtGui.QWidget):

    def assignUser(self):	
	pass	





	
    def __init__(self):

	super(VdiManagement, self).__init__()
	
	v = QtGui.QVBoxLayout()
	h = QtGui.QHBoxLayout()


	#lo = QtGui.QLabel(self)
	#lo.setPixmap(QtGui.Pixmap('louvre2.png')
	#lo.setStyleSheet("QWidget { margin-left: 1px; margin-top: 1px; }")
	#lo.setFixedWidth(100)	

	t = QtGui.QLabel("\nEnCloudEn VDI Management Utility\n")
	t.setStyleSheet("QWidget { color:#D2AE60;  font-size: 23px; font-weight: 600;}")
	t.setAlignment(Qt.AlignLeft)

	#h.addWidget(lo)
	h.addWidget(t)
	v.addLayout(h)

	
	h = QtGui.QHBoxLayout()

	l1 = QtGui.QLabel("Cluster")
        l1.setStyleSheet("QWidget { margin-left: 1px; color: #D2AE60; alignment: center; background-color: #36404A; font-size: 17px; border-radius: 5px;}")
	l1.setAlignment(Qt.AlignCenter)
	l1.setFixedWidth(200)
	l1.setFixedHeight(30)
	
	self.clusterBox = QtGui.QComboBox(self)
	self.clusterBox.setStyleSheet("QWidget { color:#D2AE60; font-size: 15px; margin-top: 5px; background-color: #36404A; text-align: center; width: 140px; height: 30px;  border-radius: 5px; font-size: 16px;}")
	self.clusterBox.setFixedWidth(200)
	self.clusterBox.setFixedHeight(35)
	self.clusterBox.addItem('172.16.253.30')
	self.clusterBox.addItem('192.168.9.50')
	self.clusterBox.move(50,50)
	
	h.addWidget(l1)		
	h.addWidget(self.clusterBox)
	v.addLayout(h)
	v.addStretch(1)

	
	h = QtGui.QHBoxLayout()
	
	l2 = QtGui.QLabel("Username")
	l2.setStyleSheet("QWidget { margin-left: 1px; color: #D2AE60; alignment: center; background-color: #36404A; font-size: 17px; border-radius: 5px;}")
        l2.setAlignment(Qt.AlignCenter)
        l2.setFixedWidth(200)
        l2.setFixedHeight(30)

	self.n1 = QtGui.QLineEdit("''")
	self.n1.setStyleSheet("QWidget { color: #000000; alignment: center; background-color: #ffffff; font-size: 16px; border-radius: 5px;}")
	self.n1.setAlignment(Qt.AlignCenter)
	self.n1.setFixedHeight(30)
	self.n1.setFixedWidth(200)
	
	h.addWidget(l2)
	h.addWidget(self.n1)
	v.addLayout(h)


        h = QtGui.QHBoxLayout()

        l3 = QtGui.QLabel("Role")
	l3.setStyleSheet("QWidget { margin-left: 1px; color: #D2AE60; alignment: center; background-color: #36404A; font-size: 17px; border-radius: 5px;}")
	l3.setAlignment(Qt.AlignCenter)
	l3.setFixedWidth(200)
	l3.setFixedHeight(30)

        self.roleBox = QtGui.QComboBox(self)
        self.roleBox.setStyleSheet("QWidget { color:#D2AE60; font-size: 15px; margin-top: 5px; background-color: #36404A; text-align: center; width: 140px; height: 30px;  border-radius: 5px; font-size: 16px;}")
        self.roleBox.setFixedWidth(200)
        self.roleBox.setFixedHeight(35)
        self.roleBox.addItem('External')
        self.roleBox.addItem('Internal')
        self.roleBox.addItem('Both')
        self.roleBox.move(50,50)

        h.addWidget(l3)
        h.addWidget(self.roleBox)
        v.addLayout(h)
        v.addStretch(1)

	
	h = QtGui.QHBoxLayout()
	v.addLayout(h)
	
	self.b1 = QtGui.QPushButton("Assign User")
	self.b1.setStyleSheet("QWidget { color:#323B44; alignment: center; font-size: 15px; margin-top: 5px; background-color: #3BAFDA; width: 60px; height: 35px; border-radius: 5px; font-weight: 600; font-size: 16px;}")
	self.b1.setFixedWidth(150)

	self.l4 = QtGui.QLabel('')
	self.l4.setStyleSheet("QWidget { color:#D2AE60; alignment: center; background-color:#2f363d; font-size: 18px; border-radius: 5px;}")
	self.l4.setAlignment(Qt.AlignCenter)
	self.l4.setFixedHeight(20)
	self.b1.setFixedWidth(200)

		
	h.addWidget(self.l4)
	h.addWidget(self.b1)
	self.b1.clicked.connect(self.assignUser)
	
		
	self.setStyleSheet("color: #ffffff;"
                    "background-color: #2f363d;"
                    "selection-color: yellow;"
                    "selection-background-color: blue;"
                    "font-weight: 300;")



	self.setLayout(v)
        self.setGeometry(0,0,300,150)
        q = self.frameGeometry()
        c = QtGui.QDesktopWidget().availableGeometry().center()
        q.moveCenter(c)
        self.move(q.topLeft())
        self.setWindowTitle('Powered By EnCloudEn')
        self.show()


    def closeEvent(self, event):

        ask = QtGui.QMessageBox.question(self, 'Quit', "Are you sure to quit?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if ask == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    ob = VdiManagement()
    sys.exit(app.exec_())
		
